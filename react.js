function greetings(firstname) {
    console.log(`hello ${firstname} !`);
}

greetings("alisha");
greetings("Todd")

const greetingsToPeople = (firstname) => {
    console.log(`hello ${firstname} !`);
}

greetingsToPeople("Johnny")

let button = document.querySelector('button')

button.addEventListener('click', () => {
    console.log("you clicked me")
});
