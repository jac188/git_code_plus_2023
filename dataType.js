let trail = {tailName: "Goose Track Trail", length: 5};
console.log(trail);

let thingsToDo = ["walk around", "feed geese", "walk across swingbridge", 1]
console.log("what are things to do:", thingsToDo)
trail.thingsToDo = thingsToDo;
trail["slogan"] = "is the trail that gives you goose bumps";
console.log("what is the trail", trail)

let openStatus = true;

let introduction = `welcome to the the ${trail.tailName}. this trail is ${trail.length} miles long one way. we can ${trail.thingsToDo[2]}. ${trail.thingsToDo[1]}, ${trail.thingsToDo[0]} on this trails. this ${trail.slogan}`

console.log(introduction)

openStatus = false;
let index = thingsToDo.indexOf("walk around")
console.log(index, thingsToDo[index])

thingsToDo.splice(index, 1)
console.log("new things to do:", thingsToDo)

let firstname = "ryan"
if (firstname === "anni"){
    console.log("you get a cat")
}
else if(firstname === "julie"){
    console.log("you get a puppy")
}
else {
    console.log("you get a fish")
}

let array = [1, 2, 3, 4, 5]
for(let i = 0; i < array.length; i++){
    console.log(array[i])
}

let dict = {a: "apple", b: "bat", c: "cat"}
for(let key in dict){
    console.log(key, dict[key])
}

let i = 4
while(i){
    console.log(i)
    i--
}

function pizzaDelivery(order) {
    console.log("deliverpizza", order)
    console.log("deliver pizza")
}

pizzaDelivery(["cheese", "ham"])